import logging

from boto.s3.connection import S3Connection, Key
from django.conf import settings
from wunderkind.celery import app

from .utils import inc_copy_progress, set_total_file_count, reset_copy_progress

logger = logging.getLogger(__name__)


@app.task
def copy_file(key):
    try:
        conn = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
        src_bucket = conn.get_bucket(settings.SRC_BUCKET_NAME)
        k = Key(src_bucket, key)
        if k.exists():
            dst_bucket = conn.get_bucket(settings.DST_BUCKET_NAME)
            dst_bucket.copy_key(k.key, src_bucket.name, k.key)
        else:
            logger.warning('File {:r} does not exists'.format(k))
    except Exception as ex:
        logger.exception('From {} to {} {}'.format(settings.SRC_BUCKET_NAME, settings.DST_BUCKET_NAME, key.key))
        raise ex
    inc_copy_progress()


@app.task
def handle_key_file(key_file):
    set_total_file_count(0)
    reset_copy_progress()
    for l, key_name in enumerate(key_file, 1):
        set_total_file_count(l)
        copy_file.apply_async(args=(key_name.strip(),))
