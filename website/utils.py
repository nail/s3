from redis import Redis


def reset_copy_progress():
    r = Redis()
    r.set('COPIED_FILES', 0)


def inc_copy_progress():
    r = Redis()
    r.incr('COPIED_FILES')


def get_copied_files_count():
    r = Redis()
    return r.get('COPIED_FILES') or 0


def set_total_file_count(count):
    r = Redis()
    r.set('TOTAL_FILES', count)


def get_total_files_count():
    r = Redis()
    return r.get('TOTAL_FILES') or 0
