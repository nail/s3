
from django.views.generic import FormView, TemplateView

from . import forms
from .tasks import handle_key_file
from .utils import get_total_files_count, get_copied_files_count


class FileUploader(FormView):
    form_class = forms.FileForm
    success_url = 'progress'
    template_name = 'website/form.html'

    def form_valid(self, form):
        handle_key_file.apply_async(args=(form.cleaned_data['file'].file,))
        return super(FileUploader, self).form_valid(form)


class ProgressView(TemplateView):
    template_name = 'website/progress.html'

    def get_context_data(self, **kwargs):
        context = super(ProgressView, self).get_context_data(**kwargs)
        context['total_files'] = get_total_files_count()
        context['processed_files'] = get_copied_files_count()
        return context
