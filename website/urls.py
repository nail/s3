from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.FileUploader.as_view(), name='form'),
    url(r'^progress/$', views.ProgressView.as_view(), name='progress')
]
