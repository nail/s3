from django.forms import forms

__all__ = ['FileForm']


class FileForm(forms.Form):
    file = forms.FileField()

    def clean_file(self):
        f = self.cleaned_data['file']
        if f.content_type == 'text/plain':
            return f
        raise forms.ValidationError('Mime type is not text/plan')
