Django==1.8.5
boto==2.38.0
celery==3.1.19
django-celery==3.1.17
redis==2.10.5